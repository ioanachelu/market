# require 'api_constraints'

Rails.application.routes.draw do
  devise_for :users
  # Api definition
  # namespace :api, defaults: { format: :json }  do
    # scope module: :v1, constraints: ApiConstraints.new(version: 1, default: true) do
  #
  namespace :api, defaults: { format: 'json' } do
    resources :users, :only => [:show, :create, :update, :destroy]
  end
  # end
end
